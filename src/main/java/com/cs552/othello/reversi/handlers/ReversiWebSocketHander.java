package com.cs552.othello.reversi.handlers;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.websocket.Session;

import com.cs552.othello.reversi.entities.ReversiGame;
import com.cs552.othello.reversi.entities.ReversiGameUser;
import com.cs552.othello.reversi.utils.ReversiConstants;
import com.cs552.othello.reversi.utils.ReversiConstants.Action;
import com.cs552.othello.reversi.utils.ReversiConstants.Color;
import com.cs552.othello.reversi.utils.ReversiUtil;
import com.cs552.othello.reversi.web.ReversiGameResponse;

public class ReversiWebSocketHander {

	private ReversiWebSocketHander() {
	}

	private static ReversiWebSocketHander instance = null;

	public static synchronized ReversiWebSocketHander getInstance() {
		if (instance == null)
			instance = new ReversiWebSocketHander();
		return instance;
	}

	private static final BlockingQueue<ReversiGameUser> waitingQueue = new ArrayBlockingQueue<>(10);
	private static final ConcurrentMap<String, ReversiGameUser> sessionMap = new ConcurrentHashMap<>();
	private static final ConcurrentMap<String, ReversiGame> gamesMap = new ConcurrentHashMap<>();

	public void addToSession(Session s, String name) {
		String sessionId = ReversiUtil.getSessionId(s);

		ReversiGameUser user = new ReversiGameUser(sessionId, s, name);
		if (sessionMap.containsKey(sessionId)) {
			user = sessionMap.get(sessionId);
			user.setSession(s);
			if (user.getGameId() != null) {
				ReversiGame game = gamesMap.get(user.getGameId());
				sendResponseMessage(user, new ReversiGameResponse(Action.RESUME, user.getColor().getValue(),
						game.getBoard(), "Resume", game.getCurrentUser().getId().equals(user.getId())));
				return;
			}
		}
		sessionMap.put(sessionId, user);

		try {
			if (waitingQueue.isEmpty()) {
				waitingQueue.put(user);
			} else {
				ReversiGameUser opponentUser = waitingQueue.take();
				// Initialize the game.
				ReversiGame game = ReversiUtil.initializeGame(opponentUser, user);
				gamesMap.put(game.getGameId(), game);
				sendResponseMessage(user, new ReversiGameResponse(user.getColor().getValue(), game.getBoard(),
						opponentUser.getName(), false));
				sendResponseMessage(opponentUser, new ReversiGameResponse(opponentUser.getColor().getValue(),
						game.getBoard(), user.getName(), true));
			}

		} catch (InterruptedException ie) {
			// TODO
		}
	}

	// TODO
	public void removeFromSession(String sessionId) {
		ReversiGameUser user = sessionMap.get(sessionId);
		ReversiGameUser opponentUser = user.getOpponentId() != null ? sessionMap.get(user.getOpponentId()) : null;
		if (opponentUser != null) {
			// TODO Send end game update.
			gamesMap.remove(sessionMap.get(sessionId).getGameId());
			sendResponseMessage(opponentUser,
					new ReversiGameResponse(ReversiConstants.Action.TERMINATE, "Opponent disconnected.", false));
			// TODO reset opponent game. Add him to the queue.
		} else {
			waitingQueue.remove(sessionMap.get(sessionId));
		}
		sessionMap.remove(sessionId);
		sessionMap.remove(opponentUser.getId());
	}

	public void handlePlay(String sessionId, int position, String message) {
		ReversiGameUser user = sessionMap.get(sessionId);
		ReversiGameUser opponentUser = sessionMap.get(user.getOpponentId());
		ReversiGame game = gamesMap.get(user.getGameId());
		Integer[] board = game.getBoard();
		if (!ReversiUtil.isValidMove(board, user.getColor().getValue(), position)) {
			game.setCurrentUser(user);
			sendResponseMessage(user,
					new ReversiGameResponse(ReversiConstants.Action.INVALID_MOVE, "Invalid Move", true));
		} else {
			// Valid Move -- Update the board.
			ReversiUtil.updateBoard(board, user.getColor().getValue(), position);
			if (ReversiUtil.isEndState(board)) {
				// Game end.
				long userCount = Arrays.stream(board).filter(i -> i != null && i == user.getColor().getValue()).count();
				String userMsg = userCount == 32 ? "Match tie" : (userCount > 32) ? "You won!" : "You lost!.";
				String oppMsg = userCount == 32 ? "Match tie" : (userCount < 32) ? "You won!" : "You lost!.";
				sendResponseMessage(user, new ReversiGameResponse(Action.END, userMsg, false));
				sendResponseMessage(opponentUser, new ReversiGameResponse(Action.END, oppMsg, false));
			} else {
				game.setCurrentUser(opponentUser);
				sendResponseMessage(opponentUser, new ReversiGameResponse("Board updated", board, true));
			}

		}

	}

	public void handleNoMove(String sessionId) {
		ReversiGameUser user = sessionMap.get(sessionId);
		ReversiGameUser opponentUser = sessionMap.get(user.getOpponentId());
		ReversiGame game = gamesMap.get(user.getGameId());
		sendResponseMessage(opponentUser, new ReversiGameResponse(Action.NO_MOVE, game.getBoard(),
				"No valid move exits for the opponent.", true));
		game.setCurrentUser(opponentUser);
	}

	public void handleRestart(String sessionId, String message) {
		ReversiGameUser user = sessionMap.get(sessionId);
		ReversiGameUser opponentUser = sessionMap.get(user.getOpponentId());
		sendResponseMessage(opponentUser, new ReversiGameResponse(Action.RESTART, "restart requested", true));
	}

	public void handleNewGame(String sessionId, String message) {
		ReversiGameUser user = sessionMap.get(sessionId);
		ReversiGameUser opponentUser = sessionMap.get(user.getOpponentId());
		// Initialize the game.
		ReversiGame game = ReversiUtil.initializeGame(opponentUser, user);
		gamesMap.put(game.getGameId(), game);
		sendResponseMessage(user, new ReversiGameResponse(Action.START, user.getColor().getValue(), game.getBoard(),
				opponentUser.getName(), user.getColor() == Color.BLACK));
		sendResponseMessage(opponentUser, new ReversiGameResponse(Action.START, opponentUser.getColor().getValue(),
				game.getBoard(), user.getName(), user.getColor() != Color.BLACK));

	}
	

	public void handleRestartDecline(String sessionId) {
		ReversiGameUser user = sessionMap.get(sessionId);
		ReversiGameUser opponentUser = sessionMap.get(user.getOpponentId());
		// Turn should not matter here.
		sendResponseMessage(opponentUser, new ReversiGameResponse(Action.RESTART_DECLINE, "Restart declined", true));
	}

	public void handleTerminateGame(String sessionId) {
		ReversiGameUser user = sessionMap.get(sessionId);
		ReversiGameUser opponentUser = sessionMap.get(user.getOpponentId());
		gamesMap.remove(user.getGameId());
		sessionMap.remove(user.getId());
		sessionMap.remove(opponentUser.getId());
		sendResponseMessage(opponentUser,
				new ReversiGameResponse(Action.TERMINATE, "Opponent terminated the game.", false));

	}

	public synchronized void sendResponseMessage(ReversiGameUser user, ReversiGameResponse response) {
		try {
			sendMessage(user, ReversiUtil.writeValueAsJSON(response));
		} catch (IOException e) {
			// TODO
			e.printStackTrace();
		}
	}

	public synchronized void sendMessage(ReversiGameUser user, String message) {
		try {
			user.getSession().getBasicRemote().sendText(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
