package com.cs552.othello.reversi.web;

import com.cs552.othello.reversi.utils.ReversiConstants;

public class ReversiGameResponse {

	private ReversiConstants.Action action;
	private int color;
	private Integer[] board;
	private String message;
	private boolean turn;

	public ReversiGameResponse(int color, Integer[] board,String message, boolean turn) {
		this.action = ReversiConstants.Action.START;
		this.color = color;
		this.board = board;
		this.message = message;
		this.turn = turn;
	}

	public ReversiGameResponse(String message, Integer[] board, boolean turn) {
		this.action = ReversiConstants.Action.PLAY;
		this.board = board;
		this.message = message;
		this.turn = turn;
	}

	public ReversiGameResponse(ReversiConstants.Action action, String message, boolean turn) {
		this.action = action;
		this.message = message;
		this.turn = turn;
	}

	public ReversiGameResponse(ReversiConstants.Action action, Integer[] board, String message, boolean turn) {
		this.action = action;
		this.board = board;
		this.message = message;
		this.turn = turn;
	}

	public ReversiGameResponse(ReversiConstants.Action action, int color, Integer[] board, String message,
			boolean turn) {
		this.action = action;
		this.board = board;
		this.message = message;
		this.turn = turn;
		this.color = color;
	}

	public ReversiConstants.Action getAction() {
		return action;
	}

	public void setAction(ReversiConstants.Action action) {
		this.action = action;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public Integer[] getBoard() {
		return board;
	}

	public void setBoard(Integer[] board) {
		this.board = board;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
