package com.cs552.othello.reversi.web;

import com.cs552.othello.reversi.utils.ReversiConstants;
import com.cs552.othello.reversi.utils.ReversiConstants.Action;

public class ReversiGameRequest {

	private ReversiConstants.Action action;
	private Integer position;
	private String message;

	public ReversiConstants.Action getAction() {
		return action;
	}

	public void setAction(ReversiConstants.Action action) {
		this.action = action;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
