package com.cs552.othello.reversi.utils;

import static com.cs552.othello.reversi.utils.ReversiConstants.COLS;
import static com.cs552.othello.reversi.utils.ReversiConstants.ROWS;

import java.io.IOException;
import java.util.Arrays;

import javax.websocket.Session;

import com.cs552.othello.reversi.entities.ReversiGame;
import com.cs552.othello.reversi.entities.ReversiGameUser;
import com.cs552.othello.reversi.utils.ReversiConstants.Color;
import com.cs552.othello.reversi.utils.ReversiConstants.Direction;
import com.cs552.othello.reversi.web.ReversiGameResponse;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ReversiUtil {

	private static ObjectMapper jsonMapper = new ObjectMapper();
	static {
		configureObjectMapper(jsonMapper);
	}

	public static <T> T readJSONValue(String jsonContent, Class<T> valueType) throws IOException {
		return readValue(jsonMapper, jsonContent, valueType);
	}

	public static <T> String writeValueAsJSON(T obj) throws IOException {
		return writeValueAsString(jsonMapper, obj);
	}

	public static <T> T readValue(ObjectMapper om, String content, Class<T> valueType) throws IOException {
		return om.readValue(content, valueType);
	}

	public static <T> String writeValueAsString(ObjectMapper om, T obj) throws IOException {
		return om.writeValueAsString(obj);
	}

	public static ReversiGame initializeGame(ReversiGameUser black, ReversiGameUser white) {
		white.setColor(Color.WHITE);
		white.setOpponentId(black.getId());
		black.setColor(Color.BLACK);
		black.setOpponentId(white.getId());
		ReversiGame game = new ReversiGame(black, white);
		black.setGameId(game.getGameId());
		white.setGameId(game.getGameId());
		black.setWaiting(false);
		white.setWaiting(false);
		return game;
	}

	public static void initializeBoard(Integer[] board) {
		for (int i = 1; i <= ROWS; i++) {
			for (int j = 1; j <= COLS; j++) {
				board[i * 10 + j] = Color.EMPTY.getValue();
			}
		}
		board[4 * 10 + 4] = board[5 * 10 + 5] = Color.BLACK.getValue();
		board[4 * 10 + 5] = board[5 * 10 + 4] = Color.WHITE.getValue();
	}

	public static ReversiGameResponse play(ReversiGameUser player, int position, ReversiGame game) {
		// Get the board
		Integer[] board = game.getBoard();
		// Check that it is a valid move
		if (!isValidMove(board, player.getColor().getValue(), position)) {
			return new ReversiGameResponse(ReversiConstants.Action.INVALID_MOVE, "Invalid Move", true);
		}
		// Update the board
		updateBoard(board, player.getColor().getValue(), position);
		return new ReversiGameResponse("Board updated", board, true);

	}

	public static boolean isValidMove(Integer[] board, int player, int position) {
		if (isValidIndex(board, position) && board[position] == Color.EMPTY.getValue()) {
			for (Direction direction : Direction.values()) {
				int newPos = position + direction.getOffset();
				if (isValidIndex(board, newPos) && board[newPos] == 1 - player) {
					do {
						newPos += direction.getOffset();
						if (!isValidIndex(board, newPos) || board[newPos] == Color.EMPTY.getValue())
							break;
						if (board[newPos] == player)
							return true;
					} while (true);
				}
			}

		}
		return false;
	}

	public static void updateBoard(Integer[] board, int player, int position) {
		board[position] = player;
		for (Direction direction : Direction.values()) {
			int newPos = position + direction.getOffset();
			if (isValidIndex(board, newPos) && board[newPos] == 1 - player) {
				do {
					newPos += direction.getOffset();
					if (!isValidIndex(board, newPos) || board[newPos] == Color.EMPTY.getValue())
						break;
					if (board[newPos] == player) {
						int tmpPos = position + direction.getOffset();
						while (tmpPos != newPos) {
							board[tmpPos] = player;
							tmpPos += direction.getOffset();
						}
						break;
					}

				} while (true);
			}
		}
	}

	public static boolean isEndState(Integer[] board) {
		return Arrays.stream(board).noneMatch(i -> i!=null && i == Color.EMPTY.getValue());
	}

	public static String getSessionId(Session s) {
		return (String) s.getUserProperties().get("sessionId");
	}

	private static boolean isValidIndex(Integer[] board, int index) {
		return (index >= 0 && index < 100 && board[index] != null);
	}

	private static void configureObjectMapper(ObjectMapper om) {
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		om.setVisibility(PropertyAccessor.FIELD, Visibility.ANY).setVisibility(PropertyAccessor.GETTER, Visibility.NONE)
				.setVisibility(PropertyAccessor.IS_GETTER, Visibility.NONE);
	}

}
