package com.cs552.othello.reversi.utils;

public class ReversiConstants {

	public static final int ROWS = 8, COLS = 8;

	public enum Color {
		BLACK(0), WHITE(1), EMPTY(-1);

		private int value;

		private Color(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	public enum Direction {
		NORTH_WEST(-11), NORTH(-10), NORTH_EAST(-9), WEST(-1), EAST(1), SOUTH_WEST(9), SOUTH(10), SOUTH_EAST(11);

		private int offset;

		private Direction(int offset) {
			this.offset = offset;
		}

		public int getOffset() {
			return offset;
		}
	}

	public enum Action {
		START, PLAY, RESUME, INVALID_MOVE, NO_MOVE, END, RESTART, NEW_GAME,TERMINATE, RESTART_DECLINE;
	}
}
