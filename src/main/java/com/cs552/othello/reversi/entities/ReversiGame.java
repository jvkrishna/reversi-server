package com.cs552.othello.reversi.entities;

import java.util.UUID;

import com.cs552.othello.reversi.utils.ReversiUtil;

public class ReversiGame {
	private final Integer[] board;
	private String gameId;
	private ReversiGameUser black;
	private ReversiGameUser white;
	private ReversiGameUser currentUser;

	public ReversiGame(ReversiGameUser black, ReversiGameUser white) {
		this.board = new Integer[10 * 10];
		this.gameId = UUID.randomUUID().toString();
		this.black = black;
		this.white = white;
		this.currentUser = black;
		ReversiUtil.initializeBoard(this.board);
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public ReversiGameUser getBlack() {
		return black;
	}

	public void setBlack(ReversiGameUser black) {
		this.black = black;
	}

	public ReversiGameUser getWhite() {
		return white;
	}

	public void setWhite(ReversiGameUser white) {
		this.white = white;
	}

	public Integer[] getBoard() {
		return board;
	}

	public ReversiGameUser getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(ReversiGameUser currentUser) {
		this.currentUser = currentUser;
	}
	
	

}
