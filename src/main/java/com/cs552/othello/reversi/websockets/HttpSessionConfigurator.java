package com.cs552.othello.reversi.websockets;

import java.util.Arrays;
import java.util.List;

import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;
import javax.websocket.server.ServerEndpointConfig.Configurator;

public class HttpSessionConfigurator extends Configurator {

	@Override
	public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
		List<String> cookies = request.getHeaders().get("Cookie");
		if (cookies != null && !cookies.isEmpty()) {
			String sessionId = Arrays.stream(cookies.get(0).split(";")).filter(s -> s.contains("sessionId="))
					.findFirst().orElse("").replace("sessionId=", "").trim();
			if (sessionId != null)
				sec.getUserProperties().put("sessionId", sessionId);
		}

	}
}
