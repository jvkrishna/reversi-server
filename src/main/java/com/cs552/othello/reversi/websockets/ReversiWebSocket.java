package com.cs552.othello.reversi.websockets;

import java.io.IOException;

import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.cs552.othello.reversi.handlers.ReversiWebSocketHander;
import com.cs552.othello.reversi.utils.ReversiUtil;
import com.cs552.othello.reversi.web.ReversiGameRequest;

@ServerEndpoint(value = "/reversi", configurator = HttpSessionConfigurator.class)
public class ReversiWebSocket {

	private ReversiWebSocketHander sessionHandler = ReversiWebSocketHander.getInstance();

	@OnOpen
	public void onOpen(Session s, EndpointConfig config) {
		System.out.println("Client opened session with session id: " + s.getId());
	}

	@OnClose
	public void onClose(Session s, EndpointConfig config) {
		System.out.println("Client session closed for session id:" + s.getUserProperties().get("sessionId"));
		sessionHandler.removeFromSession(ReversiUtil.getSessionId(s));
	}

	@OnMessage
	public void onMessage(Session s, String message, EndpointConfig config) {
		try {
			String sessionId = ReversiUtil.getSessionId(s);
			ReversiGameRequest request = ReversiUtil.readJSONValue(message, ReversiGameRequest.class);
			switch (request.getAction()) {
			case START:
				sessionHandler.addToSession(s, request.getMessage());
				break;
			case PLAY:
				sessionHandler.handlePlay(sessionId, request.getPosition(), request.getMessage());
				break;
			case NO_MOVE:
				sessionHandler.handleNoMove(sessionId);
				break;
			case END:
				// TODO
				break;
			case RESUME:
				break;
			case RESTART:
				sessionHandler.handleRestart(sessionId, request.getMessage());
				break;
			case NEW_GAME:
				sessionHandler.handleNewGame(sessionId, request.getMessage());
				break;
			case RESTART_DECLINE:
				sessionHandler.handleRestartDecline(sessionId);
				break;
			case TERMINATE:
				sessionHandler.handleTerminateGame(sessionId);
				break;
			default:
				break;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@OnError
	public void onError(Throwable e) {
		System.out.println("Error ");
		e.printStackTrace();
	}

}
